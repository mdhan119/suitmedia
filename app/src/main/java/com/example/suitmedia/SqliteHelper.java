package com.example.suitmedia;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;



public class SqliteHelper extends SQLiteOpenHelper {
    //DATABASE NAME
    public static final String DATABASE_NAME = "suitmedia";
    //DATABASE VERSION
    public static final int DATABASE_VERSION = 1;

    //TABLE NAME
    public static final String TABLE_USERS = "users";
    //TABLE USERS COLUMNS
    //ID COLUMN @primaryKey
    public static final String
        KEY_ID = "id",
        KEY_FIRST_NAME = "first_name",
        KEY_LAST_NAME = "last_name",
        KEY_IMAGE = "image",
        KEY_EMAIL = "email";

    //SQL for creating users table
    public static final String SQL_TABLE_USERS = " CREATE TABLE " + TABLE_USERS
            + " ( "
            + KEY_ID + " INTEGER PRIMARY KEY, "
            + KEY_FIRST_NAME + " TEXT, "
            + KEY_LAST_NAME + " TEXT, "
            + KEY_IMAGE + " TEXT, "
            + KEY_EMAIL + " TEXT "
            + " ) ";


    public SqliteHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        //Create Table when oncreate gets called
        sqLiteDatabase.execSQL(SQL_TABLE_USERS);

    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        //drop table to create new one if database version updated
        sqLiteDatabase.execSQL(" DROP TABLE IF EXISTS " + TABLE_USERS);
    }

    public void addUser(ContentValues values) {
        SQLiteDatabase db = this.getWritableDatabase();
        long todo_id = db.insert(TABLE_USERS, null, values);
    }
    public void editUser(ContentValues values,String id){
        SQLiteDatabase db = this.getWritableDatabase();
        long todo_id = db.update(TABLE_USERS,values,KEY_ID+" = '"+id+"'",null);
    }
    public boolean isIDExists(String id) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(TABLE_USERS,// Selecting Table
                new String[]{KEY_ID},//Selecting columns want to query
                KEY_ID + "=?",
                new String[]{id},//Where clause
                null, null, null);

        if (cursor != null && cursor.moveToFirst()&& cursor.getCount()>0) {
            //if cursor has value then in user database there is user associated with this given email so return true
            return true;
        }
        //if email does not exist return false
        return false;
    }
}
