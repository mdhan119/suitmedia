package com.example.suitmedia;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.suitmedia.guest.Adapter;
import com.example.suitmedia.guest.Pojo;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Screen4 extends AppCompatActivity implements View.OnClickListener {
    Activity activity;
    Context context;
    View root;
    String  TAG;

    //toobar
    ImageView img_toolbar_back,img_toolbar_search,img_toolbar_map,img_toolbar_list,img_toolbar_refresh;
    TextView txt_toolbar_title;

    //body
    Boolean isLoading = false;
    int page = 1,number = 10,no = 0;
    private NestedScrollView nestedSV;
    RecyclerView rv_list;
    Adapter adapter;
    ArrayList<Pojo> arrList;
    RequestQueue queue;
    SqliteHelper sqliteHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.supportRequestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_screen4);

        activity    = this;
        context     = this;
        root        = getWindow().getDecorView().getRootView();
        TAG         = new FunctionApp().getTAG(activity);
        queue       = Volley.newRequestQueue(activity);
        sqliteHelper= new SqliteHelper(activity);

        //toolbar
        txt_toolbar_title   = (TextView) root.findViewById(R.id.txt_toolbar_title);
        img_toolbar_back    = (ImageView) root.findViewById(R.id.img_toolbar_back);
        img_toolbar_search  = (ImageView) root.findViewById(R.id.img_toolbar_search);
        img_toolbar_map     = (ImageView) root.findViewById(R.id.img_toolbar_map);
        img_toolbar_list    = (ImageView) root.findViewById(R.id.img_toolbar_list);
        img_toolbar_refresh = (ImageView) root.findViewById(R.id.img_toolbar_refresh);

        txt_toolbar_title.setText("Guest");
        img_toolbar_refresh.setVisibility(View.VISIBLE);
        img_toolbar_search.setVisibility(View.VISIBLE);

        img_toolbar_search.setOnClickListener(this);
        img_toolbar_back.setOnClickListener(this);
        img_toolbar_list.setOnClickListener(this);
        img_toolbar_refresh.setOnClickListener(this);

        nestedSV    = (NestedScrollView) root.findViewById(R.id.idNestedSV);
        rv_list     = (RecyclerView) root.findViewById(R.id.rv_list);

        nestedSV.setOnScrollChangeListener((NestedScrollView.OnScrollChangeListener) (v, scrollX, scrollY, oldScrollX, oldScrollY) -> {
            new FunctionApp().logD(TAG,"scrollllll");
            if(v.getChildAt(v.getChildCount() - 1) != null) {
                if ((scrollY >= (v.getChildAt(v.getChildCount() - 1).getMeasuredHeight() - v.getMeasuredHeight())) &&
                        scrollY > oldScrollY && !isLoading) {
                    getList();
                }
            }
        });
        adapter     = new Adapter(context);
        arrList     = new ArrayList<>();
        getList();
        profileDetail();
    }

    @Override
    public void onBackPressed() {
        moveTaskToBack(true);
    }

    Boolean isButton = true;
    private void clickButton(){
        isButton = false;
        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                isButton = true;
            }
        }, 1500);
    }
    @Override
    public void onClick(View v) {
        if(isButton && v == img_toolbar_back){
            new FunctionApp().intentFinish(activity,Screen2.class,0);
        }

        if(isButton && v == img_toolbar_refresh){
            clickButton();
            refresh();
        }

        //detail profile
        if(v == profile_img_close){
            clickButton();
            dialog_profile.cancel();
        }
    }

    private void getRequest(final String p1, String url, final Map<String, String> params, int method){
        final LinearLayout ll_content   = (LinearLayout) root.findViewById(R.id.ll_content);
        new FunctionApp().setDisabled(true, ll_content, root);
        new FunctionApp().logD(TAG,"send data "+params);
        isLoading = true;
        StringRequest postRequest = new StringRequest(method, url,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {
                        // response
//                        new FunctionApp().logD(TAG, "Respon REQUEST: "+response);
                        isLoading = false;
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            if(p1.equals("list")){
                                responseList(jsonObject);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            new FunctionApp().alertInformation(activity, activity.getString(R.string.alert_internal_error));
                        }
                        new FunctionApp().setEnable(true, ll_content, root);
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        isLoading = false;
                        new FunctionApp().setEnable(true, ll_content, root);
                        new FunctionApp().logE(TAG,"Error.Response "+String.valueOf(error));
                        NetworkResponse networkResponse = error.networkResponse;
                        String responseString="";
                        if (networkResponse != null) {
                            try {
                                int error_code = networkResponse.statusCode;
                                responseString = new String(networkResponse.data, HttpHeaderParser.parseCharset(networkResponse.headers));
                                new FunctionApp().logE(TAG,responseString);
                                if(error_code == 401){
                                    JSONObject result = new JSONObject(responseString);
                                    Toast.makeText(activity,result.getString("message"), Toast.LENGTH_SHORT).show();
                                }else{
                                    new FunctionApp().alertInformation(activity, activity.getString(R.string.alert_internal_error));
                                }
                            } catch (UnsupportedEncodingException e) {
                                new FunctionApp().alertInformation(activity, activity.getString(R.string.alert_internal_error));
                                e.printStackTrace();
                            } catch (JSONException e) {
                                new FunctionApp().alertInformation(activity, activity.getString(R.string.alert_internal_error));
                                e.printStackTrace();
                            }
                        }else{
                            new FunctionApp().alertInformation(activity, activity.getString(R.string.alert_connection));
                        }
                    }
                }
        ) {
            @Override
            public Map<String, String> getParams(){
                new FunctionApp().logD(TAG,"send data "+params);
                return params;
            }
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type","application/x-www-form-urlencoded");
                return headers;
            }
        };
        queue.add(postRequest);
    }
    private void getList(){
        HashMap<String, String> params = new HashMap<String, String>();
        String url = Constant.url_list+"page="+String.valueOf(page)+"&per_page="+String.valueOf(number);
        getRequest("list", url,params, Request.Method.GET);
    }

    private void responseList(JSONObject res){
        try {
            if(res.getJSONArray("data").length()>0){
                page += 1;
                JSONArray data    = res.getJSONArray("data");
                for (int i=0;i<data.length();i++){
                    JSONObject list = data.getJSONObject(i);

                    String id           = String.valueOf(list.getInt("id"));
                    String first_name   = list.getString("first_name");
                    String last_name    = list.getString("last_name");
                    String email        = list.getString("email");
                    String image        = list.getString("avatar");

                    ContentValues values = new ContentValues();
                    values.put("id",id);
                    values.put("first_name",first_name);
                    values.put("last_name",last_name);
                    values.put("image",image);
                    values.put("email",email);

                    if(sqliteHelper.isIDExists(id)){
                        sqliteHelper.editUser(values,id);
                    }else{
                        sqliteHelper.addUser(values);
                    }

                    Pojo pojo = new Pojo();
                    pojo.setID(id);
                    pojo.setImage(image);
                    pojo.setEmail(email);
                    pojo.setFirstName(first_name);
                    pojo.setLastName(last_name);
                    arrList.add(pojo);
                }

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        LinearLayout ll_no_data = (LinearLayout) root.findViewById(R.id.ll_no_data);
        if(arrList.size()>0){
            ll_no_data.setVisibility(View.GONE);
            rv_list.setVisibility(View.VISIBLE);
        }else{
            ll_no_data.setVisibility(View.VISIBLE);
            rv_list.setVisibility(View.GONE);
        }
        adapter.setListContent(arrList);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(activity,2);
        rv_list.setLayoutManager(gridLayoutManager); // set LayoutManager to RecyclerView
        rv_list.setAdapter(adapter);

        rv_list.addOnItemTouchListener(new Adapter.RecyclerTouchListener(context,
                rv_list, new Adapter.ClickListener() {
            @Override
            public void onClick(View view, final int position) {
                LinearLayout ll_item = (LinearLayout) view.findViewById(R.id.ll_item);
                ll_item.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Pojo list = adapter.getList(position);
                        profile_txt_first_name.setText(list.getFirstName());
                        profile_txt_last_name.setText(list.getLastName());
                        profile_txt_email.setText(list.getEmail());
                        String url = list.getImage();
                        Picasso.get()
                                .load(url)
                                .placeholder(R.drawable.avatar)
                                .error(R.drawable.avatar)
                                .into(profile_img);
                        dialog_profile.show();
                    }
                });
            }
            @Override
            public void onLongClick(View view, int position) {
            }
        }));

    }

    BottomSheetDialog dialog_profile;
    ImageView profile_img_close,profile_img;
    TextView profile_txt_first_name,profile_txt_last_name,profile_txt_email;
    private void profileDetail(){
        dialog_profile = new BottomSheetDialog(context);
        View view = root.inflate(activity, R.layout.detail_profile, null);

        profile_img_close   = (ImageView) view.findViewById(R.id.img_close);
        profile_img         = (ImageView) view.findViewById(R.id.img);
        profile_txt_first_name  = (TextView) view.findViewById(R.id.txt_first_name);
        profile_txt_last_name   = (TextView) view.findViewById(R.id.txt_last_name);
        profile_txt_email       = (TextView) view.findViewById(R.id.txt_email);

        profile_img_close.setOnClickListener(this);
        dialog_profile.setContentView(view);
    }

    private void refresh(){
        adapter.removeAll();
        adapter = new Adapter(context);
        arrList = new ArrayList<>();
        no = 0;
        page = 1;
        getList();
    }
}