package com.example.suitmedia.event;

import org.json.JSONObject;

import java.util.ArrayList;

public class Pojo {

    //POJO class consists of get method and set method
    private String id,name,image,desc,date,time;
    private ArrayList<Pojo> customPojo =new ArrayList<>();

    public String getID(){return id;}
    public void setID(String value){this.id=value;}

    public String getName(){return name;}
    public void setName(String value){this.name=value;}

    public String getImage(){return image;}
    public void setImage(String value){this.image=value;}

    public String getDesc(){return desc;}
    public void setDesc(String value){this.desc=value;}

    public String getDate(){return date;}
    public void setDate(String value){this.date=value;}

    public String getTime(){return time;}
    public void setTime(String value){this.time=value;}
}
