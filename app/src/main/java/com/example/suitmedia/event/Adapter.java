package com.example.suitmedia.event;

import android.content.Context;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.example.suitmedia.FunctionApp;
import com.example.suitmedia.R;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
public class Adapter extends RecyclerView.Adapter<Adapter.MyViewHolder> {

    private ArrayList<Pojo> list_data=new ArrayList<>();
    private final LayoutInflater inflater;
    View view;
    MyViewHolder holder;
    private Context context;


    public Adapter(Context context){
        this.context=context;
        inflater= LayoutInflater.from(context);
    }
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        view=inflater.inflate(R.layout.list_event, parent, false);
        holder=new MyViewHolder(view);
        return holder;
    }

    //Binding the data using get() method of POJO object
    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        Pojo list_items=list_data.get(position);
        holder.txt_name.setText(list_items.getName());
        holder.txt_desc.setText(list_items.getDesc());
        holder.txt_date.setText(list_items.getDate());
        holder.txt_time.setText(list_items.getTime());
    }

    //Setting the arraylist
    public void setListContent(ArrayList<Pojo> list_data){
        this.list_data=list_data;
        notifyItemRangeChanged(0,list_data.size());
    }

    @Override
    public int getItemCount() {
        return list_data.size();
    }

    //View holder class, where all view components are defined
    class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        TextView txt_name,txt_desc,txt_date,txt_time;
        public MyViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            txt_name    = (TextView) itemView.findViewById(R.id.txt_name);
            txt_desc    = (TextView) itemView.findViewById(R.id.txt_desc);
            txt_date    = (TextView) itemView.findViewById(R.id.txt_date);
            txt_time    = (TextView) itemView.findViewById(R.id.txt_time);
        }

        @Override
        public void onClick(View v) {

        }
    }

    public Pojo getList(int position){
        Pojo list = list_data.get(position);
        return list;
    }
    public void updateList(int position,Pojo list){
        list_data.set(position, list);
        notifyDataSetChanged();
    }

    public void removeAt(int position) {
        try {
            list_data.remove(position);
            notifyItemRemoved(position);
            notifyItemRangeChanged(position, list_data.size());
        }catch (Exception e){
            new FunctionApp().logE("Adapter", "error data : "+ e.toString());
        }
    }
    public void removeAll(){
        try {
            int size = this.list_data.size();
            if (size > 0) {
                for (int i = 0; i < size; i++) {
                    this.list_data.remove(0);
                }
                this.notifyItemRangeRemoved(0, size);
            }
        }catch (Exception e){
            new FunctionApp().logE("Adapter", "error data : "+ e.toString());
        }
    }
    public static interface ClickListener{
        public void onClick(View view, int position);
        public void onLongClick(View view, int position);
    }

    public static class RecyclerTouchListener implements RecyclerView.OnItemTouchListener{

        private ClickListener clicklistener;
        private GestureDetector gestureDetector;

        public RecyclerTouchListener(Context context, final RecyclerView recycleView, final ClickListener clicklistener){

            this.clicklistener=clicklistener;
            gestureDetector=new GestureDetector(context,new GestureDetector.SimpleOnGestureListener(){
                @Override
                public boolean onSingleTapUp(MotionEvent e) {
                    return true;
                }

                @Override
                public void onLongPress(MotionEvent e) {
                    View child=recycleView.findChildViewUnder(e.getX(),e.getY());
                    if(child!=null && clicklistener!=null){
                        clicklistener.onLongClick(child,recycleView.getChildAdapterPosition(child));
                    }
                }
            });
        }

        @Override
        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
            View child=rv.findChildViewUnder(e.getX(),e.getY());
            if(child!=null && clicklistener!=null && gestureDetector.onTouchEvent(e)){
                clicklistener.onClick(child,rv.getChildAdapterPosition(child));
            }

            return false;
        }

        @Override
        public void onTouchEvent(RecyclerView rv, MotionEvent e) {

        }

        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

        }
    }

    public String getName(int p){
        String value = list_data.get(p).getName();
        return value;
    }
    public String getID(int p){
        String value = list_data.get(p).getID();
        return value;
    }
}