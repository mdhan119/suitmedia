package com.example.suitmedia;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.suitmedia.event.Adapter;
import com.example.suitmedia.event.Pojo;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class Screen3 extends AppCompatActivity implements View.OnClickListener {

    Activity activity;
    Context context;
    View root;
    String  TAG;

    //toobar
    ImageView img_toolbar_back,img_toolbar_search,img_toolbar_map,img_toolbar_list,img_toolbar_refresh;
    TextView txt_toolbar_title;

    //body
    Boolean isLoading = false;
    int page = 1,number = 10,no = 0;
    private NestedScrollView nestedSV;
    RecyclerView rv_list;
    Adapter adapter;
    ArrayList<Pojo> arrList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.supportRequestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_screen3);

        activity    = this;
        context     = this;
        root        = getWindow().getDecorView().getRootView();
        TAG         = new FunctionApp().getTAG(activity);

        //toolbar
        img_toolbar_back    = (ImageView) root.findViewById(R.id.img_toolbar_back);
        img_toolbar_search  = (ImageView) root.findViewById(R.id.img_toolbar_search);
        img_toolbar_map     = (ImageView) root.findViewById(R.id.img_toolbar_map);
        img_toolbar_list    = (ImageView) root.findViewById(R.id.img_toolbar_list);
        img_toolbar_refresh = (ImageView) root.findViewById(R.id.img_toolbar_refresh);

        img_toolbar_refresh.setVisibility(View.VISIBLE);
        img_toolbar_map.setVisibility(View.VISIBLE);
        img_toolbar_search.setVisibility(View.VISIBLE);

        img_toolbar_search.setOnClickListener(this);
        img_toolbar_map.setOnClickListener(this);
        img_toolbar_back.setOnClickListener(this);
        img_toolbar_list.setOnClickListener(this);
        img_toolbar_refresh.setOnClickListener(this);

        //body
        nestedSV    = (NestedScrollView) root.findViewById(R.id.idNestedSV);
        rv_list     = (RecyclerView) root.findViewById(R.id.rv_list);
        nestedSV.setOnScrollChangeListener((NestedScrollView.OnScrollChangeListener) (v, scrollX, scrollY, oldScrollX, oldScrollY) -> {
            new FunctionApp().logD(TAG,"scrollllll");
            if(v.getChildAt(v.getChildCount() - 1) != null) {
                if ((scrollY >= (v.getChildAt(v.getChildCount() - 1).getMeasuredHeight() - v.getMeasuredHeight())) &&
                        scrollY > oldScrollY && !isLoading) {
                    getList();
                }
            }
        });

        adapter     = new Adapter(context);
        arrList     = new ArrayList<>();
        getList();
    }

    @Override
    public void onBackPressed() {
        moveTaskToBack(true);
    }

    Boolean isButton = true;
    private void clickButton(){
        isButton = false;
        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                isButton = true;
            }
        }, 1500);
    }

    @Override
    public void onClick(View v) {
        if(isButton && v == img_toolbar_back){
            new FunctionApp().intentFinish(activity,Screen2.class,0);
        }
        if(isButton && v == img_toolbar_refresh){
            clickButton();
            refresh();
        }
        if(isButton && v == img_toolbar_map){
            clickButton();
            new FunctionApp().alertInformation(activity, "I am so sory, i don't have api map key for view a maps");
        }
    }

    private void getList(){
        isLoading = true;
        final LinearLayout ll_content   = (LinearLayout) root.findViewById(R.id.ll_content);
        new FunctionApp().setDisabled(true, ll_content, root);

        for (int i=1; i<=number;i++){
            no ++;
            Date c = Calendar.getInstance().getTime();
            SimpleDateFormat df  = new SimpleDateFormat("dd-MMM-yyyy", Locale.getDefault());
            SimpleDateFormat df2 = new SimpleDateFormat("HH:mm", Locale.getDefault());
            String date = df.format(c);
            String time = df2.format(c);

            Pojo pojo = new Pojo();
            pojo.setID(String.valueOf(no));
            pojo.setName("Card Title "+no);
            pojo.setDesc(Constant.desc);
            pojo.setDate(date);
            pojo.setTime(time);
            arrList.add(pojo);
        }
        adapter.setListContent(arrList);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(activity,1);
        rv_list.setLayoutManager(gridLayoutManager); // set LayoutManager to RecyclerView
        rv_list.setAdapter(adapter);
        isLoading = false;
        new FunctionApp().setEnable(true, ll_content, root);
    }
    private void refresh(){
        adapter.removeAll();
        adapter = new Adapter(context);
        arrList = new ArrayList<>();
        no = 0;
        getList();
    }
}