package com.example.suitmedia;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

import org.json.JSONArray;
import org.json.JSONException;

import static java.lang.Boolean.FALSE;
import static java.lang.Boolean.TRUE;

public class SessionManager {

    SharedPreferences pref_user,pref_guest;
    SharedPreferences.Editor editor_user,editor_guest;

    // Context
    Context context;
    Activity activity;
    int PRIVATE_MODE = 0;

    //shared name
    public static final String User      = "user";

    // Key User
    private static final String
        LoginUser       = "loginUser"
        ;

    public SessionManager(Context context, String type) {
        this.context = context;
        if (type.equals(User)){
            pref_user   = this.context.getSharedPreferences(User, PRIVATE_MODE);
            editor_user = pref_user.edit();
        }
    }

    public void Login(String name,String palindrome){
        editor_user.putBoolean(LoginUser, TRUE);
        editor_user.putString(Constant.name,name);
        editor_user.putString(Constant.palindrome,palindrome);
        editor_user.commit();
    }

    public Boolean isLogin(){
        return pref_user.getBoolean(LoginUser,FALSE);
    }
    public String getName(){
        return pref_user.getString(Constant.name,"");
    }
    public String getPalindrome(){
        return pref_user.getString(Constant.palindrome,"");
    }

    public void Logout(){
        editor_user.putBoolean(LoginUser, FALSE);
        editor_user.remove(Constant.palindrome);
        editor_user.remove(Constant.name);
        editor_user.remove(Constant.image);
        editor_user.commit();
    }
}
