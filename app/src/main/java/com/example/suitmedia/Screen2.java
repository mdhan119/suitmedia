package com.example.suitmedia;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

public class Screen2 extends AppCompatActivity implements View.OnClickListener {

    Activity activity;
    Context context;
    View root;
    String  TAG;

    TextView txt_name;
    Button btn_event,btn_guest;

    SessionManager session_user;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.supportRequestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_screen2);

        activity    = this;
        context     = this;
        root        = getWindow().getDecorView().getRootView();
        TAG         = new FunctionApp().getTAG(activity);
        session_user= new SessionManager(activity,"user");

        txt_name    = (TextView) root.findViewById(R.id.txt_name);
        btn_event   = (Button) root.findViewById(R.id.btn_event);
        btn_guest   = (Button) root.findViewById(R.id.btn_guest);

        if(session_user.isLogin()){
            txt_name.setText(session_user.getName());
        }
        btn_event.setOnClickListener(this);
        btn_guest.setOnClickListener(this);
    }

    @Override
    public void onBackPressed() {
        moveTaskToBack(true);
    }

    Boolean isButton = true;
    private void clickButton(){
        isButton = false;
        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                isButton = true;
            }
        }, 1500);
    }
    @Override
    public void onClick(View v) {
        if(isButton && v == btn_event){
            clickButton();
            new FunctionApp().intentFinish(activity,Screen3.class,1);
        }
        if(isButton && v == btn_guest){
            clickButton();
            new FunctionApp().intentFinish(activity,Screen4.class,1);
        }
    }
}