package com.example.suitmedia;

public class Constant {
    public final static String
        name        = "name",
        image       = "image",
        palindrome  = "palindrome",
        app_name    = "Suitmedia",
        desc        = "Lorem ipsum dolor sit amet, consectetur adipiscing elit."
        ;

    public final static String base_url = "https://reqres.in/api/";
    public final static String
    url_list   = base_url+"users?" //users?page=1&per_page=10
    ;
}
