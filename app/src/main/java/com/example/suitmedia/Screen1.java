package com.example.suitmedia;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.util.Base64;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.android.material.bottomsheet.BottomSheetDialog;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

public class Screen1 extends AppCompatActivity implements View.OnClickListener {

    Activity activity;
    Context context;
    View    root;
    String  TAG;

    ImageView img_profile;
    EditText edt_palindrome,edt_name;
    Button btn_check,btn_next;

    SessionManager session_user;
    Uri imageUri= null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.supportRequestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_screeen1);
        activity    = this;
        context     = this;
        root        = getWindow().getDecorView().getRootView();
        TAG         = new FunctionApp().getTAG(activity);
        session_user= new SessionManager(activity,"user");

        img_profile = (ImageView) root.findViewById(R.id.img_profile);
        edt_name    = (EditText) root.findViewById(R.id.edt_name);
        edt_palindrome = (EditText) root.findViewById(R.id.edt_palindrome);
        btn_check   = (Button) root.findViewById(R.id.btn_check);
        btn_next    = (Button) root.findViewById(R.id.btn_next);

        ChooseFile();
        if(session_user.isLogin()){
            edt_name.setText(session_user.getName());
            edt_palindrome.setText(session_user.getPalindrome());
            imageUri = new FunctionApp().getImageStorage();
            if(imageUri != null){
                img_profile.setImageURI(imageUri);
            }
        }

        img_profile.setOnClickListener(this);
        btn_check.setOnClickListener(this);
        btn_next.setOnClickListener(this);
    }

    BottomSheetDialog dialog_gallery;
    ImageView gallery_img_close;
    LinearLayout gallery_ll_camera,gallery_ll_media;
    String[] permission_camera = new String[]{
            Manifest.permission.CAMERA,
    };
    String[] permission_gallery = new String[]{
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.READ_EXTERNAL_STORAGE,
    };
    private static final int
            PERMISSIONS_MEDIA = 10,
            PERMISSIONS_CAMERA = 11,
            REQUEST_CAMERA = 12,
            REQUEST_MEDIA = 13;
    private void ChooseFile(){
        dialog_gallery = new BottomSheetDialog(context);
        View alert_gallery = root.inflate(activity, R.layout.gallery, null);

        gallery_img_close   = (ImageView) alert_gallery.findViewById(R.id.img_close);
        gallery_ll_camera   = (LinearLayout) alert_gallery.findViewById(R.id.ll_camera);
        gallery_ll_media    = (LinearLayout) alert_gallery.findViewById(R.id.ll_media);

        gallery_img_close.setOnClickListener(this);
        gallery_ll_camera.setOnClickListener(this);
        gallery_ll_media.setOnClickListener(this);

        dialog_gallery.setContentView(alert_gallery);
    }

    Boolean isButton = true;
    private void ClickButton(){
        isButton = false;
        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                isButton = true;
            }
        }, 1500);
    }

    @Override
    public void onClick(View v) {
        if (isButton && v == img_profile){
            dialog_gallery.show();
        }
        if(isButton && v == btn_check){
            ClickButton();
            checkPalindrome();
        }
        if(isButton && v == btn_next){
            ClickButton();
            nextButton();
        }

        // Gallery
        if(v == gallery_img_close){
            ClickButton();
            dialog_gallery.cancel();
        }
        if(v == gallery_ll_camera){
            ClickButton();
            if(PermissionCamera()){
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(intent, REQUEST_CAMERA);
            }
        }
        if(v == gallery_ll_media){
            ClickButton();
            if(PermissionGallery()){
                Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                intent.setType("image/*");
                startActivityForResult(Intent.createChooser(intent, "Select File"), REQUEST_MEDIA);
            }
        }
    }

    private  boolean PermissionCamera() {
        int result;
        List<String> listPermissionsNeeded = new ArrayList<>();
        for (String p:permission_camera) {
            result = ContextCompat.checkSelfPermission(getApplicationContext(),p);
            if (result != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(p);
            }
        }
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(this, listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]),PERMISSIONS_CAMERA );
            return false;
        }
        return true;
    }

    private  boolean PermissionGallery() {
        int result;
        List<String> listPermissionsNeeded = new ArrayList<>();
        for (String p:permission_gallery) {
            result = ContextCompat.checkSelfPermission(getApplicationContext(),p);
            if (result != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(p);
            }
        }
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(this, listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]),PERMISSIONS_MEDIA );
            return false;
        }
        return true;
    }

    JSONObject data_media    = new JSONObject();
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CAMERA){
            if (resultCode == RESULT_OK){
                Uri uri = data.getData();
                Bundle bundle = data.getExtras();
                Bitmap bitmap = (Bitmap) bundle.get("data");
                String Byte = new FunctionApp().BitMapToString(bitmap);
                Random i = new Random();
                int d = i.nextInt(9999999);
                String name = String.valueOf(d);
                String extension = "png";
                try {
                    img_profile.setImageBitmap(bitmap);
                    dialog_gallery.cancel();

                    data_media.put("type", "Camera");
                    data_media.put("extension", extension);
                    data_media.put("byte", Byte);
                    new FunctionApp().logD(TAG, ""+data_media.toString());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
        if (requestCode == REQUEST_MEDIA) {
            if (resultCode == RESULT_OK) {
                String filepatch = data.getData().getPath();
                String[] s_file = filepatch.split("/");
                s_file = s_file[s_file.length-1].split(":");
                Uri uri = data.getData();
                String selectedFilePath = FilePath.getPath(this, uri);
                if(selectedFilePath == null){
                    Toast.makeText(context,activity.getString(R.string.validate_media_not_found), Toast.LENGTH_SHORT).show();
                }else{
                    File file = new File(selectedFilePath);
                    String Byte = new FunctionApp().FileToString(file);
                    String name = s_file[s_file.length-1];
                    String extension = new FunctionApp().getMimeType(context,uri);

                    try {

                        dialog_gallery.cancel();
                        img_profile.setImageURI(uri);

                        data_media.put("type", "Media");
                        data_media.put("extension", extension);
                        data_media.put("byte", Byte);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }
        }
    }

    //palindrome
    private void checkPalindrome(){
        char[] charInput = edt_palindrome.getText().toString().toCharArray();
        int intLength = charInput.length;
        boolean isPalindrome = true;
        for (int i=0; i<intLength/2; i++){
            if (charInput[i] != charInput[intLength-1-i]){
                isPalindrome = false;
                break;
            }
        }
        if (isPalindrome){
            new FunctionApp().alertInformation(activity,"Palindrome");
        }
        else {
            new FunctionApp().alertInformation(activity,"Not Palindrome");
        }
    }

    //next button
    private void nextButton(){
        if(validate()){
            try {
                if(data_media.length()>0){
                    SaveImage(data_media.getString("byte"));
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            session_user.Login(edt_name.getText().toString().trim(),edt_palindrome.getText().toString().trim());
            new FunctionApp().intentFinish(activity,Screen2.class,1);
        }
    }
    private Boolean validate(){
        Boolean status  = true;
        String str_name = edt_name.getText().toString().trim();
        String str_palindrome = edt_palindrome.getText().toString().trim();
        if(imageUri == null && data_media.length()<=0){
            status = false;
            new FunctionApp().alertInformation(activity,"Please select image");
        }else if(str_name.isEmpty()){
            status = false;
            new FunctionApp().alertInformation(activity,"input name can`t be empty");
        }else if(str_palindrome.isEmpty()){
            status = false;
            new FunctionApp().alertInformation(activity,"input palindrome can`t be empty");
        }
        return status;
    }

    public void SaveImage(String imgString) {
        try {
            byte[] data = Base64.decode(imgString, Base64.DEFAULT);
            Bitmap bitmap = BitmapFactory.decodeByteArray(data, 0, data.length);
            File mydir = new File(Environment.getExternalStorageDirectory() + "/"+Constant.app_name);
            if (!mydir.exists()) {
                mydir.mkdirs();
            }

            String fileUri = mydir.getAbsolutePath() + File.separator + "Profile.jpg";
            FileOutputStream outputStream = new FileOutputStream(fileUri);

            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, outputStream);
            outputStream.flush();
            outputStream.close();
        } catch(IOException e) {
            e.printStackTrace();
        }
    }
}