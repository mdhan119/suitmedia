package com.example.suitmedia;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Environment;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.MimeTypeMap;
import android.widget.LinearLayout;

import androidx.appcompat.app.AlertDialog;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

public class FunctionApp {
    public void logD(String TAG, String text){
        Log.d(TAG,text);
    }
    public  void logE(String TAG, String text){
        Log.e(TAG, text);
    }
    public String getTAG(Activity activity) {
        return activity.getClass().getSimpleName();
    }

    public void intentFinish(Activity activity, Class p1, int option){
        Intent i = new Intent(activity,p1);
        activity.startActivity(i);
        if(option == 0){ //close
            anim_leave(activity);
        }else{ // open
            anim_enter(activity);
        }
    }
    public void anim_enter(Activity activity){
//        activity.overridePendingTransition(R.anim.left,
//                R.anim.right);
        activity.finish();
    }
    public void anim_leave(Activity activity){
//        activity.overridePendingTransition(R.anim.left_end,
//                R.anim.rigth_end);
        activity.finish();
    }

    public String BitMapToString(Bitmap bitmap){
        ByteArrayOutputStream baos=new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG,100, baos);
        byte [] b=baos.toByteArray();
        String temp= Base64.encodeToString(b, Base64.DEFAULT);
        return temp;
    }

    public String FileToString(File file){
        int size = (int) file.length();
        byte[] bytes = new byte[size];
        try {
            bytes = loadFile(file);
            String encodedString = Base64.encodeToString(bytes, Base64.DEFAULT);
            return encodedString;
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return "";
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return "";
        }
    }
    public static byte[] loadFile(File file) throws IOException {
        InputStream is = new FileInputStream(file);

        long length = file.length();
        if (length > Integer.MAX_VALUE) {
            // File is too large
        }
        byte[] bytes = new byte[(int)length];

        int offset = 0;
        int numRead = 0;
        while (offset < bytes.length
                && (numRead=is.read(bytes, offset, bytes.length-offset)) >= 0) {
            offset += numRead;
        }

        if (offset < bytes.length) {
            throw new IOException("Could not completely read file "+file.getName());
        }

        is.close();
        return bytes;
    }

    public String getMimeType(Context context, Uri uri) {
        String extension;

        //Check uri format to avoid null
        if (uri.getScheme().equals(ContentResolver.SCHEME_CONTENT)) {
            //If scheme is a content
            final MimeTypeMap mime = MimeTypeMap.getSingleton();
            extension = mime.getExtensionFromMimeType(context.getContentResolver().getType(uri));
        } else {
            //If scheme is a File
            //This will replace white spaces with %20 and also other special characters. This will avoid returning null values on file name with spaces and special characters.
            extension = MimeTypeMap.getFileExtensionFromUrl(Uri.fromFile(new File(uri.getPath())).toString());

        }

        return extension;
    }

    public void alertInformation(Activity activity, String message){
        int design = R.style.alertDialog;
        AlertDialog dialogInfo;
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(activity,design);
        dialogBuilder.setCancelable(false);
        dialogBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                dialog.cancel();
            }
        });
        dialogBuilder.setTitle("Information");
        dialogBuilder.setMessage(message);
        dialogInfo = dialogBuilder.create();
        dialogInfo.show();
    }

    public Uri getImageStorage(){
        String fileName = "Profile.jpg";
        String completePath = Environment.getExternalStorageDirectory() + "/"+Constant.app_name+"/" + fileName;

        File file = new File(completePath);
        Uri imageUri = null;
        if(file.exists()){
            imageUri = Uri.fromFile(file);
        }

        return imageUri;
    }

    //enable and disable when loading get data from/to web server
    public void setDisabled(boolean enable, ViewGroup vg, View v){
        LinearLayout ll_progresbar  = (LinearLayout) v.findViewById(R.id.ll_progresbar);

        for (int i = 0; i < vg.getChildCount(); i++){
            View child = vg.getChildAt(i);
            if (child instanceof ViewGroup){
                setDisabled(enable, (ViewGroup)child, v);
            } else {
                child.setEnabled(false);
            }
        }
        ll_progresbar.setVisibility(View.VISIBLE);
    }
    public void setEnable(boolean enable, ViewGroup vg, View v){
        LinearLayout ll_progresbar  = (LinearLayout) v.findViewById(R.id.ll_progresbar);

        for (int i = 0; i < vg.getChildCount(); i++){
            View child = vg.getChildAt(i);
            if (child instanceof ViewGroup){
                setEnable(enable, (ViewGroup)child, v);
            } else {
                child.setEnabled(true);
            }
        }
        ll_progresbar.setVisibility(View.GONE);
    }
    //end enable and disable when loading get data from/to web server

}
