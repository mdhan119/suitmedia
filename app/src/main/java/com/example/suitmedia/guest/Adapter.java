package com.example.suitmedia.guest;

import android.content.Context;
import android.media.Image;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.example.suitmedia.FunctionApp;
import com.example.suitmedia.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class Adapter extends RecyclerView.Adapter<Adapter.MyViewHolder> {

    private ArrayList<Pojo> list_data=new ArrayList<>();
    private final LayoutInflater inflater;
    View view;
    MyViewHolder holder;
    private Context context;


    public Adapter(Context context){
        this.context=context;
        inflater= LayoutInflater.from(context);
    }
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        view=inflater.inflate(R.layout.list_guest, parent, false);
        holder=new MyViewHolder(view);
        return holder;
    }

    //Binding the data using get() method of POJO object
    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        Pojo list_items=list_data.get(position);
        holder.txt_name.setText(list_items.getFirstName()+" "+list_items.getLastName());
        String url = list_items.getImage();
        Picasso.get()
                .load(url)
                .placeholder(R.drawable.avatar)
                .error(R.drawable.avatar)
                .into(holder.img);
    }

    //Setting the arraylist
    public void setListContent(ArrayList<Pojo> list_data){
        this.list_data=list_data;
        notifyItemRangeChanged(0,list_data.size());
    }

    @Override
    public int getItemCount() {
        return list_data.size();
    }

    //View holder class, where all view components are defined
    class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        TextView txt_name;
        ImageView img;
        LinearLayout ll_item;
        public MyViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            img         = (ImageView) itemView.findViewById(R.id.img);
            txt_name    = (TextView) itemView.findViewById(R.id.txt_name);
            ll_item     = (LinearLayout) itemView.findViewById(R.id.ll_item);

            ll_item.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {

        }
    }

    public Pojo getList(int position){
        Pojo list = list_data.get(position);
        return list;
    }
    public void updateList(int position, Pojo list){
        list_data.set(position, list);
        notifyDataSetChanged();
    }

    public void removeAt(int position) {
        try {
            list_data.remove(position);
            notifyItemRemoved(position);
            notifyItemRangeChanged(position, list_data.size());
        }catch (Exception e){
            new FunctionApp().logE("Adapter", "error data : "+ e.toString());
        }
    }
    public void removeAll(){
        try {
            int size = this.list_data.size();
            if (size > 0) {
                for (int i = 0; i < size; i++) {
                    this.list_data.remove(0);
                }
                this.notifyItemRangeRemoved(0, size);
            }
        }catch (Exception e){
            new FunctionApp().logE("Adapter", "error data : "+ e.toString());
        }
    }
    public static interface ClickListener{
        public void onClick(View view, int position);
        public void onLongClick(View view, int position);
    }

    public static class RecyclerTouchListener implements RecyclerView.OnItemTouchListener{

        private ClickListener clicklistener;
        private GestureDetector gestureDetector;

        public RecyclerTouchListener(Context context, final RecyclerView recycleView, final ClickListener clicklistener){

            this.clicklistener=clicklistener;
            gestureDetector=new GestureDetector(context,new GestureDetector.SimpleOnGestureListener(){
                @Override
                public boolean onSingleTapUp(MotionEvent e) {
                    return true;
                }

                @Override
                public void onLongPress(MotionEvent e) {
                    View child=recycleView.findChildViewUnder(e.getX(),e.getY());
                    if(child!=null && clicklistener!=null){
                        clicklistener.onLongClick(child,recycleView.getChildAdapterPosition(child));
                    }
                }
            });
        }

        @Override
        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
            View child=rv.findChildViewUnder(e.getX(),e.getY());
            if(child!=null && clicklistener!=null && gestureDetector.onTouchEvent(e)){
                clicklistener.onClick(child,rv.getChildAdapterPosition(child));
            }

            return false;
        }

        @Override
        public void onTouchEvent(RecyclerView rv, MotionEvent e) {

        }

        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

        }
    }

    public String getID(int p){
        String value = list_data.get(p).getID();
        return value;
    }
}