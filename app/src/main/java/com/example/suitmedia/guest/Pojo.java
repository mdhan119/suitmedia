package com.example.suitmedia.guest;

import java.util.ArrayList;

public class Pojo {

    //POJO class consists of get method and set method
    private String id,first_name,last_name,image,email;
    private ArrayList<Pojo> customPojo =new ArrayList<>();

    public String getID(){return id;}
    public void setID(String value){this.id=value;}

    public String getImage(){return image;}
    public void setImage(String value){this.image=value;}

    public String getFirstName(){return first_name;}
    public void setFirstName(String value){this.first_name=value;}

    public String getLastName(){return last_name;}
    public void setLastName(String value){this.last_name=value;}

    public String getEmail(){return email;}
    public void setEmail(String value){this.email=value;}
}
